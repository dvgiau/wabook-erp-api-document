'use strict';


/**
 * Get data from Wabooks
 * This API use Authorize by API KEY in cookie with cookie name 'JSESSIONID'
 *
 * body MRECGateway_body  (optional)
 * jSONData String  (optional)
 * returns inline_response_200_1
 **/
exports.mRECGatewayPOST = function(body,jSONData) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = "";
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

