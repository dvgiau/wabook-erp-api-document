'use strict';


/**
 * API Key cookies name 'JSESSIONID'
 * This Api support get API Key Set-Cookies 'JSESSIONID'. B/c Swagger not support show 'Set-Cookies' in ResponseHeader
 *
 * body Waerp_getCookies_body  (optional)
 * returns inline_response_200
 **/
exports.waerpGetCookiesPOST = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "MSG" : "MSG",
  "METHOD" : "METHOD",
  "JSESSIONID" : "JSESSIONID"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

