'use strict';

var utils = require('../utils/writer.js');
var WabooksErpAPI = require('../service/WabooksErpAPIService');

module.exports.mRECGatewayPOST = function mRECGatewayPOST (req, res, next, body, jSONData) {
  WabooksErpAPI.mRECGatewayPOST(body, jSONData)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
