'use strict';

var utils = require('../utils/writer.js');
var GetCookiesForAuthorize = require('../service/GetCookiesForAuthorizeService');

module.exports.waerpGetCookiesPOST = function waerpGetCookiesPOST (req, res, next, body) {
  GetCookiesForAuthorize.waerpGetCookiesPOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
